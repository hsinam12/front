import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { PropertyComponent } from './property/property.component';
import { AgGridModule } from 'ag-grid-angular';
import { AddpropertyComponent } from './addproperty/addproperty.component';
import { AgmCoreModule } from '@agm/core';
import { UserlistComponent } from './userlist/userlist.component';
import { AddusersComponent } from './addusers/addusers.component';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { ActionLinkRendererComponent } from './actionlinkrenderer/actionlinkrenderer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    PropertyComponent,
    AddpropertyComponent,
    UserlistComponent,
    AddusersComponent,
    ActionLinkRendererComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RouterModule,
    AgGridModule.withComponents([]),
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyBWNNl1s9Sz1z-RGJtLe4MYcOLqaWZwpio'
    }),
  ],
  exports : [],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
