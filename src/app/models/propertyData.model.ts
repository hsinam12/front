import { UserData } from './userData.model';
import { GeoCoOrdinates } from './geocoordinates.model';

export class propertyData
{
  apartmentId:number;
  createdBy: UserData;
  description:string;
  geoCoOrdinates: GeoCoOrdinates;
  name:string;
  noOfRooms:string;
  pricePerMonth: string;
  size: string;
  date:string;
  state:string;
}

export class propertyDataDisplay
{
    apartmentId:number;
    name:string;
    description:string;
    size:string;
    pricePerMonth:string;
    noOfRooms:string;
    geoCoOrdinates:string;
    date:string;
    user:string;
    state:string;
}

export class propertyDataDisplayFormatter
{
    public static GetDisplayFormatted(propertydata: propertyData) : propertyDataDisplay
    {
        let propdatadisplay: propertyDataDisplay = new propertyDataDisplay();
        propdatadisplay.apartmentId = propertydata.apartmentId;
        propdatadisplay.name = propertydata.name;
        propdatadisplay.description = propertydata.description;
        propdatadisplay.size = propertydata.size;
        propdatadisplay.pricePerMonth = propertydata.pricePerMonth;
        propdatadisplay.noOfRooms = propertydata.noOfRooms;
        propdatadisplay.geoCoOrdinates = propertydata.geoCoOrdinates.latitude + ', ' + propertydata.geoCoOrdinates.longitude;
        propdatadisplay.date = propertydata.date;
        propdatadisplay.user = propertydata.createdBy.firstName + ' ' + propertydata.createdBy.lastName;
        propdatadisplay.state = (<boolean><unknown>propertydata.state)?'Occupied':'Vacant';
        return propdatadisplay;
    }

    public static GetListDisplayFormatted(propertydata: propertyData[]): propertyDataDisplay[]
    {
        let lstpropdatadisplay: propertyDataDisplay[]=[];
        propertydata.forEach(propdata => {
            lstpropdatadisplay.push(propertyDataDisplayFormatter.GetDisplayFormatted(propdata));
        });
        return lstpropdatadisplay;
    }
}


