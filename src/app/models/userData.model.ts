export class UserData
{   
    emailId:string;
    firstName:string;
    lastName:string;
    password:string;
    role:string;
}


export class UserDataDisplay
{
    Id: string;
    emailId:string;
    firstName:string;
    lastName:string;
    role:string;
}


export class UserDataDisplayFormatter
{
    public static GetDisplayFormatted(userdata: UserData) : UserDataDisplay
    {
        let userdatadisplay: UserDataDisplay = new UserDataDisplay();
        userdatadisplay.Id = userdata.emailId;
        userdatadisplay.firstName = userdata.firstName;
        userdatadisplay.lastName = userdata.lastName;
        userdatadisplay.emailId = userdata.emailId;
        userdatadisplay.role = userdata.role;
        return userdatadisplay;
    }

    public static GetListDisplayFormatted(userdata: UserData[]): UserDataDisplay[]
    {
        let lstuserdatadisplay: UserDataDisplay[]=[];
        userdata.forEach(userdata => {
            lstuserdatadisplay.push(UserDataDisplayFormatter.GetDisplayFormatted(userdata));
        });
        return lstuserdatadisplay;
    }
}