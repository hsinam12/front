import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PropertyService } from '../services/property.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { MapsAPILoader, MouseEvent, AgmMap } from '@agm/core';
import { propertyData } from '../models/propertyData.model';
import { TokenStorageService } from '../services/token-storage.service';
import { UserData } from '../models/userData.model';
import { GeoCoOrdinates } from '../models/geocoordinates.model';

@Component({
  selector: 'app-addproperty',
  templateUrl: './addproperty.component.html',
  styleUrls: ['./addproperty.component.css']
})
export class AddpropertyComponent implements OnInit {

  propertyForm: FormGroup;
  submitted = false;
  // lat: number;
  // lng: number;
  zoom: number;
  id: any;

  geocoordinates: GeoCoOrdinates = {latitude: 28.644800, longitude: 77.216721};
 

  constructor(private formBuilder: FormBuilder,private activeroute: ActivatedRoute, private propertyservice: PropertyService, private router:Router,private mapsAPILoader: MapsAPILoader, private tokenService : TokenStorageService) {
    this.propertyForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['',Validators.required],
      size: ['', [Validators.required, Validators.pattern("[0-9]+([\.][0-9]+)?")]],
      pricePerMonth: ['', [Validators.required, Validators.pattern("[0-9]+([\.][0-9]+)?")]],
      noOfRooms: ['', [Validators.required, Validators.pattern("[0-9]+")]],
      geoCoOrdinates	: ['', [Validators.required, Validators.pattern("[0-9]+([\.][0-9]+)?,[0-9]+([\.][0-9]+)?")]],
      state : [''],
    });
  }

  ngOnInit(): void {
    this.setCurrentLocation();
    this.id = this.activeroute.snapshot.paramMap.get('id');
   
    if(this.id){
      console.log(this.id);
      this.propertyservice.getPropertyById(this.id).subscribe((res:any)=>{
        console.log('success');

        let propertyData: propertyData = res;
        this.propertyForm.patchValue({
          apartmentId: propertyData.apartmentId,
          name : propertyData.name,
          description : propertyData.description,
          size : propertyData.size,
          noOfRooms: propertyData.noOfRooms,
          state: propertyData.state,
          pricePerMonth : propertyData.pricePerMonth,
          geoCoOrdinates : propertyData.geoCoOrdinates.latitude + ',' + propertyData.geoCoOrdinates.longitude
        });
        this.geocoordinates.latitude = propertyData.geoCoOrdinates.latitude;
        this.geocoordinates.longitude = propertyData.geoCoOrdinates.longitude;
        this.zoom = 15;

      }, err=>{
        console.log(err);
      })
    }
    else
    {
      this.id = 0;
      this.propertyForm.patchValue({
      geoCoOrdinates : this.geocoordinates.latitude + ',' + this.geocoordinates.longitude
      });
    }

  }

  get f() { return this.propertyForm.controls; }

  //add property
  onSubmit() {
    console.log(this.propertyForm.value);
    this.submitted = true;
    if (this.propertyForm.invalid) {
      return;
    }
    const geocoord: any = this.propertyForm.value.geoCoOrdinates.toString().split(',', 2);
    let curruser: UserData = <UserData>this.tokenService.getUser();

    let propData: any = {
        apartmentId : this.id? 0 : this.id,
        createdBy : curruser.emailId,
        date : Date.now().toString(),
        description : this.propertyForm.value.description,
        geoCoOrdinates : {latitude: geocoord[0], longitude : geocoord[1]},
        name : this.propertyForm.value.name,
        noOfRooms : this.propertyForm.value.noOfRooms,
        pricePerMonth : this.propertyForm.value.pricePerMonth,
        size : this.propertyForm.value.size,
        state : this.propertyForm.value.state
    };

    console.log(propData);
    

   
    this.propertyservice.addProperty(propData).subscribe((res: any) => {
      console.log(res);
      this.router.navigate(['/property']);
    }, err => {
      console.log(err);
    });

  }


  // set current location
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.geocoordinates.latitude = position.coords.latitude;
        this.geocoordinates.longitude = position.coords.longitude;
        this.zoom = 15;
        this.propertyForm.patchValue({geoCoOrdinates: this.geocoordinates.latitude + ',' + this.geocoordinates.longitude});
      });
    }
  }

  markerDragEnd($event: MouseEvent) {
    this.geocoordinates.latitude = $event.coords.lat;
    this.geocoordinates.longitude = $event.coords.lng;
    this.propertyForm.patchValue({geoCoOrdinates: this.geocoordinates.latitude + ',' + this.geocoordinates.longitude});
  }
}
