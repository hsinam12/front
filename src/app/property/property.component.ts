import { Component, OnInit, ViewChild } from '@angular/core';
import { ColDef, GridOptions, CellClickedEvent } from 'ag-grid-community';
import { PropertyService } from '../services/property.service';
import { propertyData, propertyDataDisplay, propertyDataDisplayFormatter } from '../models/propertyData.model';
import { UserRole } from '../enumuserrole';
import { AuthService } from '../services/auth.service';
import { TokenStorageService } from '../services/token-storage.service';
import { GeoCoOrdinates } from '../models/geocoordinates.model';
import { Router } from '@angular/router';
import { ActionLinkRendererComponent } from '../actionlinkrenderer/actionlinkrenderer.component';
declare var $: any;

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {

  columnDefs = [
    {headerName: 'ID', field : 'apartmentId', colId : 'apartmentId'},
    {headerName: 'Name', field: 'name', filter: false},
    {headerName: 'Description', field: 'description', filter: false},
    {headerName: 'Floor area size', field: 'size', sortable: true, filter: 'agTextColumnFilter', filterParams: { debounceMs: 1000 }},
    {headerName: 'Price per month', field: 'pricePerMonth', sortable: true, filter: 'agTextColumnFilter', filterParams: { debounceMs: 1000 }},
    {headerName: 'Number of rooms', field: 'noOfRooms', sortable: true, filter: 'agTextColumnFilter', filterParams: { debounceMs: 0 }},
    {headerName: 'geoCoOrdinates', field: 'geoCoOrdinates', filter: false},
    {headerName: 'Date added', field: 'date', sortable: true, filter: 'date', suppress: true,
      filterParams: {
          comparator: function(filterLocalDateAtMidnight, cellValue) {
              var dateAsString = cellValue;
              var dateParts = dateAsString.split('/');
              var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

              if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                  return 0;
              }

              if (cellDate < filterLocalDateAtMidnight) {
                  return -1;
              }

              if (cellDate > filterLocalDateAtMidnight) {
                  return 1;
              }
          }
      },
      floatingFilterComponentParams: {
        suppressFilterButton: true
      }
    },
    {headerName: 'Associated realtor', field: 'user', sortable: true, filter: 'agTextColumnFilter', filterParams: { debounceMs: 2000 }},
    {headerName: 'State', field: 'state', filter : false},
    {headerName: 'Actions', colId: 'actions', filter: false,  width: 165, pinned: 'right', cellRenderer: 'buttonRenderer',
    cellRendererParams: {
      onEditClick: this.onBtnClickEdit.bind(this),
      onDeleteClick: this.onBtnClickDelete.bind(this),
      labelEdit: 'Edit',
      labelDelete : 'Delete',
      HasEditAction: true
    }
  },   
];

rowData : propertyDataDisplay[] = [
    // { apartmentId: 1, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Occupied"},
    // { apartmentId: 2, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Vacant"},
    // { apartmentId: 3, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Occupied"},
    // { apartmentId: 4, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Occupied"},
    // { apartmentId: 5, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Occupied"},
    // { apartmentId: 6, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Occupied"},
    // { apartmentId: 7, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Vacant"},
    // { apartmentId: 8, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Occupied"},
    // { apartmentId: 9, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Occupied"},
    // { apartmentId: 10, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Vacant"},
    // { apartmentId: 11, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Vacant"},
    // { apartmentId: 12, name: 'Celica', description: "no any description", size:"600sq" , pricePerMonth: "12000", noOfRooms: "3", geoCoOrdinates:"546557.6876", date:"10/05/2020", user:"admin", state: "Occupied"},
];

gridOptions : GridOptions = {
  defaultColDef :
  {
    filter: true,
    floatingFilter: true
  },
  pagination: true,
  paginationPageSize : 20
};

selectedPropertyId: number;
userrole: UserRole;
AllowActions: boolean = false;

ShowMap: boolean = false;
ShowTable: boolean = true;

geocoordinates: GeoCoOrdinates = {latitude: 28.644800, longitude: 77.216721};
listGeoCoOrdinates: GeoCoOrdinates[] = [this.geocoordinates];
zoom:number = 15;

frameworkComponents: any;

  constructor(private propertyservice:PropertyService, private authservice: AuthService, private tokenservice: TokenStorageService, private router: Router) { 
    this.frameworkComponents = {
      buttonRenderer: ActionLinkRendererComponent
    }

    this.gridOptions.onGridReady = () => {
      this.gridOptions.columnApi.setColumnVisible('apartmentId', false);
  
      this.authservice.UserLoggedOut.subscribe((isUserLoggedOut:boolean) => {
        this.AllowActions = !isUserLoggedOut;
      });
  
      if(this.userrole == UserRole.Client)
      {
        this.gridOptions.columnApi.setColumnVisible('actions', false);
        this.AllowActions = false;
      }
      else{
        this.AllowActions = true;
      }
    };
}

  ngOnInit(): void 
  {
    this.userrole = <UserRole>this.tokenservice.getRole();

    this.FillData();

}

  private FillData(): void
  {
    this.propertyservice.getallProperty().subscribe((res:propertyData[])=>{
      console.log('success');
      if(res.length > 0)
      {
        this.rowData = propertyDataDisplayFormatter.GetListDisplayFormatted(res);
      }
    }, err=>{
      console.log(err);
    })

  }

  onCancelbutton() {
    $(document).ready(function() {
      $('#delete-property').hide();
    });
  }

  onSubmitbutton()
  {
    $(document).ready(function() {
      $('#delete-property').hide();
  });

  this.propertyservice.deleteProperty(this.selectedPropertyId).subscribe((res:any)=>{
    console.log('success');
    console.log(res.data);
    this.FillData();
  }, err=>{
    console.log(err);
    this.FillData();
  })

}



  OnDeleteLinkClick(id: number) {
    console.log('OnDeleteLinkClick');
    $(document).ready(function() {
      $('#delete-property').show();
    });
    this.selectedPropertyId = id;
  }


  OnEditLinkClick(id: number) {
    console.log('OnEditLinkClicked');
    this.router.navigate(['/property/' + id]);
  }

  onShowAllOnMapClicked()
  {
    if(this.ShowMap == false){
      this.ShowMap = true;
      this.ShowTable = false;
    }else{
      this.ShowMap = false;
      this.ShowTable = true;
    }
    
    this.propertyservice.getallProperty().subscribe((res:propertyData[])=>{
      console.log('success');
      if(res.length > 0)
      {
        this.listGeoCoOrdinates = [];
        res.forEach(propdata => {
          this.listGeoCoOrdinates.push(propdata.geoCoOrdinates);
        });
      }
    }, err=>{
      console.log(err);
    })

    var map = $('#googleMap');
    map.focus();


  }

  onBtnClickEdit(e) {
    this.OnEditLinkClick(e.rowData.apartmentId);
    
  }
  
  onBtnClickDelete(e) {
    this.OnDeleteLinkClick(e.rowData.apartmentId);
    
  }

}
