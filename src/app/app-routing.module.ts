import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PropertyComponent } from './property/property.component';
import { AddpropertyComponent } from './addproperty/addproperty.component';
import { UserlistComponent } from './userlist/userlist.component';
import { AddusersComponent } from './addusers/addusers.component';

const routes: Routes = [
  { path: '', component : LoginComponent },
  { path: 'login', component : LoginComponent },
  { path: 'register', component : RegisterComponent },
  { path: 'property', component : PropertyComponent },
  { path: 'newproperty', component: AddpropertyComponent },
  { path: 'property/:id', component: AddpropertyComponent },
  { path: 'users', component: UserlistComponent },
  { path: 'newuser', component: AddusersComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
