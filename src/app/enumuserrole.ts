export enum UserRole{
    Admin = "ADMIN",
    Realtor = "REALTOR",
    Client = "CLIENT"
}