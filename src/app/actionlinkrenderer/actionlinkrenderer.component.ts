import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';

@Component({
templateUrl : "./actionlinkrenderer.component.html"
})



export class ActionLinkRendererComponent implements AgRendererComponent{

params: any;
labelEdit: string;
labelDelete:string;
HasEditAction:boolean;


    refresh(params: any): boolean {
        return false;
    }

    agInit(params: import("ag-grid-community").ICellRendererParams): void {
        this.params = params;
        this.labelEdit = this.params.labelEdit;
        this.labelDelete = this.params.labelDelete;
        this.HasEditAction = this.params.HasEditAction;
        console.log(this.params);
    }

    onEditClick($event) {
        if (this.params.onEditClick instanceof Function) {
          // put anything into params u want pass into parents component
          const params = {
            event: $event,
            rowData: this.params.node.data
            // ...something
          }
          this.params.onEditClick(params);
    
        }
    }


        onDeleteClick($event) {
            if (this.params.onDeleteClick instanceof Function) {
              const params = {
                event: $event,
                rowData: this.params.node.data
              }
              this.params.onDeleteClick(params);
        
            }
        }
}