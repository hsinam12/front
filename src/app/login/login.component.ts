import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { UserRole } from '../enumuserrole';
import { TokenStorageService } from '../services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  forgetpasswordDiv = false;
  loginDiv = true;
  constructor(private formBuilder: FormBuilder, private authservice: AuthService, private router: Router, private tokenservice: TokenStorageService) {
    tokenservice.signOut();
   }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['',Validators.required],
   });
  }

  get f() { return this.loginForm.controls; }

  forgotButton(){
    this.forgetpasswordDiv = true;
    this.loginDiv = false;
  }

  onSubmit(){
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.authservice.loginUser(this.loginForm.value).subscribe((res: any) => {
        this.tokenservice.saveToken(res.jwt);
console.log(res.jwt);
        this.authservice.getUserById(this.loginForm.value.username).subscribe((resuser: any) =>
        {
          this.tokenservice.saveUser(resuser);
          this.router.navigate(['/property']);
        },
        erruser =>
        {
          console.log('error retrieving session user\n' + erruser);
        });

      this.authservice.UserAuthenticated.emit(res.role);
    }, err => {
      console.log('error authenticating user\n' + err);
    });
  }

}
