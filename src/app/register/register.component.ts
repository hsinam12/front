import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  inputdata: any;
  data: any;

  constructor(private formBuilder: FormBuilder, private authservice: AuthService, private router:Router) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      emailId: ['', Validators.required],
      password: ['',Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
   });
  }

  get f() { return this.registerForm.controls; }

  // register user
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    this.inputdata = this.registerForm.value;

    this.data = {
      emailId : this.inputdata.emailId,
      password : this.inputdata.password,
      firstName : this.inputdata.firstName,
      lastName : this.inputdata.lastName,
      role : "CLIENT"
    }

    console.log(this.data);

    this.authservice.registerUser(this.data).subscribe((res: any) => {
      console.log('registration successful.');
    }, err => {
      console.log(err);
    });
  }
}
