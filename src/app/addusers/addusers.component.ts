import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { UserRole } from '../enumuserrole';

@Component({
  selector: 'app-addusers',
  templateUrl: './addusers.component.html',
  styleUrls: ['./addusers.component.css']
})
export class AddusersComponent implements OnInit {
  addUserForm: FormGroup;
  submitted = false;

  listuserRoles = UserRole;
  keys = Object.keys(this.listuserRoles);

  constructor(private formBuilder: FormBuilder, private authservice: AuthService, private router:Router) { 
    }


  ngOnInit(): void {
    this.addUserForm = this.formBuilder.group({
      emailId: ['', Validators.required],
      password: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: ['', Validators.required],
      role	: ['', Validators.required],
   });
  }

  get f() { return this.addUserForm.controls; }

  onSubmit() {
    console.log(this.addUserForm.value);
    this.submitted = true;
    if (this.addUserForm.invalid) {
      return;
    }

    this.authservice.registerUser(this.addUserForm.value).subscribe((res: any) => {
      console.log(res);
    }, err => {
      console.log(err);
    });
  }


}
