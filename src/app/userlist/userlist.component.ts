import { Component, OnInit } from '@angular/core';
import { ColDef, GridOptions, CellClickedEvent } from 'ag-grid-community';
import { AuthService } from '../services/auth.service';
import { TokenStorageService } from '../services/token-storage.service';
import { UserRole } from '../enumuserrole';
import { UserData, UserDataDisplayFormatter } from '../models/userData.model';
import { ActionLinkRendererComponent } from '../actionlinkrenderer/actionlinkrenderer.component';
declare var $: any;

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  columnDefs = [
    {headerName: 'ID', field : 'Id', colId : 'Id'},
    {headerName: 'User ID', field: 'emailId'},
    {headerName: 'First lastName', field: 'firstName'},
    {headerName: 'Last lastName', field: 'lastName'},
    {headerName: 'Email Id', field: 'emailId'},
    {headerName: 'Role', field: 'role'},
    {headerName: 'Actions', colId: 'actions', pinned: 'right',cellRenderer: 'buttonRenderer',
    cellRendererParams: {
      onDeleteClick: this.onBtnClickDelete.bind(this), 
      labelEdit: 'Edit',
      labelDelete : 'Delete',
      HasEditAction: false
    }
  }];

rowData = [
  // { Id: 'celica@pleximus.com', firstName: 'Celica', lastName: 'Celica', emailId: 'celica@pleximus.com', role: "realtor"},
  // { Id: 'celica@pleximus.com', firstName: "Celica", lastName: 'Celica', emailId: " celica@pleximus.com ", role: "realtor"},
  // { Id: 'celica@pleximus.com', firstName: "Celica", lastName: 'Celica', emailId: " celica@pleximus.com ", role: "realtor"},
  // { Id: 'celica@pleximus.com', firstName: "Celica", lastName: 'Celica', emailId: " celica@pleximus.com ", role: "realtor"},
  // { Id: 'celica@pleximus.com', firstName: "Celica", lastName: 'Celica', emailId: " celica@pleximus.com ", role: "realtor"},
  // { Id: 'celica@pleximus.com', firstName: "Celica", lastName: 'Celica', emailId: " celica@pleximus.com ", role: "realtor"},
];


gridOptions: GridOptions = {};

selectedUserId: string;
userrole: UserRole;
AllowActions: boolean = false;

frameworkComponents: any;


    constructor(private authservice: AuthService, private tokenservice: TokenStorageService) {
      this.frameworkComponents = {
        buttonRenderer: ActionLinkRendererComponent
      }
      this.gridOptions.onGridReady = () => {
      this.gridOptions.columnApi.setColumnVisible('Id', false);

      if(this.userrole != UserRole.Admin)
      {
        this.gridOptions.columnApi.setColumnVisible('actions', false);
        this.AllowActions = false;
      }
      else{
        this.AllowActions = true;
      }
    };
  }

  ngOnInit(): void {
    this.userrole = <UserRole>this.tokenservice.getRole();

    this.FillData();
  }

  private FillData(): void
  {
    this.authservice.getAllUsers().subscribe((res:UserData[])=>{
      console.log('success');
      if(res.length > 0)
      {
        this.rowData = UserDataDisplayFormatter.GetListDisplayFormatted(res);
      }
    }, err=>{
      console.log(err);
    })

  }

  onCellClicked($event: CellClickedEvent) {
    $(document).ready(function() {
      $('#delete-record').show();
    });
}

  onCancelbutton() {
    $(document).ready(function() {
      $('#delete-record').hide();
    });
  }

  onSubmitbutton()
  {
      $(document).ready(function() {
        $('#delete-record').hide();
    });

    this.authservice.deleteUser(this.selectedUserId).subscribe((res:any)=>{
      console.log('success');
      console.log(res.data);
    }, err=>{
      console.log(err);
      this.FillData();
    });

    
  }

    OnDeleteLinkClick(id: string) {
      console.log('OnDeleteLinkClick'+id);
      $(document).ready(function() {
        $('#delete-record').show();
      });
      this.selectedUserId = id;
    }


    onBtnClickEdit(e) {
    
    }
    
    onBtnClickDelete(e) {
      this.OnDeleteLinkClick(e.rowData.Id);
    }
  
  }
