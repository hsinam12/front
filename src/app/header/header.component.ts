import { Component, OnInit } from '@angular/core';
import { UserRole } from '../enumuserrole';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { TokenStorageService } from '../services/token-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  IsUserAuthenticated: boolean = false;
  IsAdminUser: boolean = false;
  constructor(private authservice: AuthService, private router: Router, private tokenservice : TokenStorageService) {
    
   }

  ngOnInit(): void {
    this.authservice.UserAuthenticated.subscribe(
        (userrole: string) =>
        {
          this.IsAdminUser = false;
          this.IsUserAuthenticated = true;
          if(userrole == UserRole.Admin)
          {
            this.IsAdminUser = true;
          }
        });
  }

  Logout()
  {
    this.tokenservice.signOut();
    this.IsUserAuthenticated = false;
    this.IsAdminUser = false;
    this.authservice.UserLoggedOut.emit(true);
    this.router.navigate(['']);
  }

}
