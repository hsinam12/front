import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError, tap } from 'rxjs/operators';
import { UserData } from '../models/userData.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  UserAuthenticated = new EventEmitter<string>();
  UserLoggedOut = new EventEmitter<boolean>();

  httpHeaders : HttpHeaders;

  constructor(private http: HttpClient) {
    this.httpHeaders = new HttpHeaders({
      'Content-Type':  'application/json',
    });
   }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }

  // register user 
  registerUser(data){
     return this.http.put(environment.url + '/user/save',  data, { headers: this.httpHeaders })
     .pipe(map(this.extractData));
  }

  // login
  loginUser(data){
    return this.http.post(environment.url + '/authenticate',  data, { headers: this.httpHeaders })
    .pipe(map(this.extractData));
  }

  // call by admin
  saveUser(data){
    return this.http.put(environment.url + 'user/save', data, { headers: this.httpHeaders })
    .pipe(map(this.extractData));
  }

  getAllUsers(){
    return this.http.get<UserData[]>(environment.url + '/user/all', { headers: this.httpHeaders })
    .pipe<UserData[]>(map((data: UserData[]) => data));
  }

  deleteUser(id){
    return this.http.delete(environment.url + '/delete/user/'+ id, { headers: this.httpHeaders })
    .pipe(map(this.extractData));
  } 

  getUserById(id){
    return this.http.get<UserData>(environment.url + '/user/'+ id, { headers: this.httpHeaders })
    .pipe<UserData>(map((data: UserData) => {return data}));
  }
}
