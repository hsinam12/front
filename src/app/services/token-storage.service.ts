import { Injectable } from '@angular/core';
import { UserData, UserDataDisplayFormatter } from '../models/userData.model';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  signOut() {
    window.localStorage.clear();
  }

  public saveToken(token: string) {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  public getRole() {
    let userData: UserData = <UserData>this.getUser();
    console.log('tokenuser : ' + userData);
    console.log(userData.role);
    return userData.role;
  }

 public saveUser(user)
 {
  window.localStorage.removeItem(USER_KEY);
  window.localStorage.setItem(USER_KEY, JSON.stringify(user));
  console.log('user session stored.');
 }

 public getUser()
 {
  return JSON.parse(localStorage.getItem(USER_KEY));
 }

}