import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, catchError, tap } from 'rxjs/operators';
import { propertyData } from '../models/propertyData.model';
import { throwError, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  httpHeaders : HttpHeaders;

  resultpropertyData = new Subject<propertyData>();

  constructor(private http: HttpClient) { 

    this.httpHeaders = new HttpHeaders({
      'Content-Type':  'application/json'
    });
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }

  addProperty(data:propertyData){
    return this.http.put(environment.url + '/apartment/save', data, { headers: this.httpHeaders })
    .pipe(map(this.extractData));
  }

  editProperty(data:propertyData){
    return this.http.put(environment.url + '/apartment/save', data, { headers: this.httpHeaders })
    .pipe(map(this.extractData));
  }

  deleteProperty(id){
    return this.http.delete(environment.url + '/delete/apartment/'+ id, { headers: this.httpHeaders })
    .pipe(map(this.extractData));
  }

  getallProperty(){
    return this.http.get<propertyData[]>(environment.url + '/apartment/all', { headers: this.httpHeaders })
    .pipe<propertyData[]>(map((data: propertyData[]) => {return data}));
  }

  getPropertyById(id){
    return this.http.get<propertyData>(environment.url + '/apartment/'+ id, { headers: this.httpHeaders })
    .pipe<propertyData>(map((data: propertyData) => {return data}));
  }


  private handleError(errorRes: HttpErrorResponse) {
    return throwError('Error occured');
  }

}

